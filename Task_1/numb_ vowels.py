word = str(input("enter text: "))
vowels = ['a', 'i', 'u', 'e', 'o', 'y']
num_vowels = 0
for ch in word:
    if ch in vowels:
        num_vowels += 1

print("number of vowels in word is ", num_vowels)
