isbn = str(input("type 9 digits: "))
numb = int(isbn)
sum = 0
for i in range(2, 11, 1):
    digit = numb % 10
    numb //= 10
    sum += i * digit

last = (11 - sum % 11) % 11
print("ISBN: "+isbn + 'X' if last == 10 else str(last))
