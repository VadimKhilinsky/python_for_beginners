def celsius_to_fahrenheit(celsius):
    return 1.8 * celsius + 32


def ms_to_mile_per_hour(ms):
    return 2.23694 * ms


def fahrenheit_to_celcius(fahrenheit):
    return (fahrenheit - 32) / 1.8


def effective_air_temperature(t, speed):
    return 35.74 + 0.6215 * t + (0.4275 * t - 35.75) * speed ** 0.16


if __name__ == "__main__":
    celcius_temp = float(input("type temperature (\u2103) : "))
    wind_speed_ms = float(input(r"type wind speed (m\s) : "))
    fahrenheit_temp = celsius_to_fahrenheit(celcius_temp)
    wind_speed_mile_per_hour = ms_to_mile_per_hour(wind_speed_ms)

    if abs(fahrenheit_temp) > 50 or wind_speed_mile_per_hour < 3 or wind_speed_mile_per_hour > 120:
        print("warning.. calculations may be inaccurate")

    print(fahrenheit_to_celcius(effective_air_temperature(fahrenheit_temp, wind_speed_mile_per_hour)))
