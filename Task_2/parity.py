def parity(x, y):
    if not x&1 and not y&1 :
        return x*y
    elif x&1 and y&1:
        return x+y
    else:
        return x if x&1 else y


if __name__ == "__main__":
    print(parity(2,4))
    print(parity(3,2))
    print(parity(1,5))
