def concat(text, delimiter=" "):
    return delimiter.join(text.split(" "))


if __name__ == "__main__":
    text = str(input("type text: "))
    delimiter = str(input("type delimiter: "))
    print(concat(text,delimiter))
