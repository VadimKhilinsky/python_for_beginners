import datetime


def is_work_day(day):
    if day > 365 or day < 1:
        return
    day_of_week = (datetime.datetime(2019, 1, 1) + datetime.timedelta(numb_of_day - 1)).isoweekday()
    if day_of_week < 6:
        return "workday"
    else:
        return "day off"


if __name__ == "__main__":
    numb_of_day = int(input())

    print(is_work_day(numb_of_day))
