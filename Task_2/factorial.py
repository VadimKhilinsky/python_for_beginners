def f(numb):
    if numb <= 1:
        return 1
    return numb * f(numb - 1)


# maximal recursion with N = 998
if __name__ == "__main__":
    print(f(100))
